package com.springboot.converter.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class User {

    @ExcelProperty(value = {"用户信息","姓名"},index = 0)
    private String name;

    @ExcelProperty(value = {"用户信息","年龄"},index = 1)
    private Integer age;

    @ExcelProperty(value = {"用户信息","性别"},index = 5)
    private String sex;

    @ExcelProperty(value = {"用户信息","电话"},index = 3)
    private String tel;

    @ExcelProperty(value = {"用户信息","城市"},index = 4)
    private String city;

    @ExcelProperty(value = {"用户信息","头像"},index = 2)
    private String avatar;
}
