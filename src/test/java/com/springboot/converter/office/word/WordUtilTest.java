package com.springboot.converter.office.word;

import cn.afterturn.easypoi.entity.ImageEntity;
import com.springboot.converter.office.chat.JFreeChartUtil;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class WordUtilTest {

    @Test
    void export() {
        Map<String, Object> map = new HashMap<String, Object>();

        putBaseInfo(map);
        putImg(map);
        putList(map);
        try {
            putBar(map);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            zhuzhuangtu(map);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        WordUtil.exportDocx("export.docx", "E:\\data\\","export1.docx",map,"东北再担保有限责任公司");
    }

    @Test
    void export1() {
        Map<String, Object> map = new HashMap<String, Object>();

        putBaseInfo(map);
        putImg(map);
        putList(map);
        try {
            putBar(map);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            zhuzhuangtu(map);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        WordUtil.exportDocument("E:\\gitlab\\converter-spring-boot-starter\\src\\main\\resources\\templates\\word\\export1.docx", "E:\\data\\","testdoc.doc",map,"",WordUtil.DOC_TYPE);
    }

    private static void putBaseInfo(Map<String, Object> map) {
        map.put("department", "Easypoi");
        map.put("person", "zhangsan");
        map.put("time", "234234");
    }

    private static void putImg(Map<String, Object> map) {
        ImageEntity image = new ImageEntity();
        image.setHeight(200);
        image.setWidth(500);
        image.setUrl("E:\\gitlab\\converter-spring-boot-starter\\src\\main\\resources\\templates\\images\\1.jpeg");
        image.setType(ImageEntity.URL);
        map.put("img2", image);
    }

    private static void putBar(Map<String, Object> map) throws IOException {

        File file2 = File.createTempFile("temp", "jpg");


        DefaultPieDataset pds = new DefaultPieDataset();

        pds.setValue("上市公司股票", 100);
        pds.setValue("非上市公司股权", 200);
        pds.setValue("传统不良债权", 300);
        pds.setValue("抵债资产", 400);
        pds.setValue("投资性房地产", 500);
        pds.setValue("长期股权投资", 600);

        JFreeChartUtil.createPieChart(pds, file2, "账面价值比例");

        ImageEntity image = new ImageEntity();
        image.setHeight(200);
        image.setWidth(500);
        System.out.println(file2.getAbsolutePath());
        image.setUrl(file2.getAbsolutePath());
        image.setType(ImageEntity.URL);
        map.put("img1", image);
    }

    public static void zhuzhuangtu(Map<String, Object> dataMap) throws IOException {
        File file2 = File.createTempFile("temp", "jpg");
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();


        List<String> nameList = new ArrayList<>();
        nameList.add("广东省");
        nameList.add("河南省");
        nameList.add("内蒙古自治区");
        nameList.add("黑龙江省");
        nameList.add("新疆");
        nameList.add("湖北省");
        nameList.add("辽宁省");
        nameList.add("山东省");
        nameList.add("陕西省");
        nameList.add("上海市");
        nameList.add("贵州省");
        nameList.add("重庆市");
        nameList.add("西藏自治区");
        nameList.add("安徽省");
        nameList.add("福建省");
        nameList.add("湖南省");
        nameList.add("海南省");
        nameList.add("江苏省");
        nameList.add("广西");
        nameList.add("宁夏");
        nameList.add("青海省");
        nameList.add("江西省");
        nameList.add("浙江省");
        nameList.add("山西省");
        nameList.add("四川省");
        nameList.add("香港特别行政区");
        nameList.add("澳门特别行政区");
        nameList.add("云南省");
        nameList.add("北京市");
        nameList.add("天津市");
        nameList.add("吉林省");


        List<String> areaList = new ArrayList<>();

        nameList.forEach(i -> {
            if (i.contains("省")) {
                areaList.add(i.replace("省", ""));
            } else if (i.contains("特别行政区")) {
                areaList.add(i.replace("特别行政区", ""));
            } else if (i.contains("市")) {
                areaList.add(i.replace("市", ""));
            } else if (i.contains("自治区")) {
                areaList.add(i.replace("自治区", ""));
            } else {
                areaList.add(i);
            }
        });


        areaList.forEach(i -> {
            dataset.setValue(23434, "账面价值", i);
        });

        areaList.forEach(i -> {
            dataset.setValue(34234234, "估值结果", i);
        });

        JFreeChartUtil.createBarChart(dataset, file2, "资产结果");
        ImageEntity image = new ImageEntity();
        image.setHeight(200);
        image.setWidth(500);
        System.out.println(file2.getAbsolutePath());
        image.setUrl(file2.getAbsolutePath());
        image.setType(ImageEntity.URL);
        dataMap.put("img", image);
    }

    private static void putList(Map<String, Object> map) {
        List<Map<String, String>> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Map<String, String> map1 = new HashMap<>();
            map1.put("name", "xiao");
            map1.put("age", "12");

            list.add(map1);
        }

        map.put("list", list);
    }

    @Test
    void convertWordToPdf() {
        WordUtil.convertWordToPdf("E:\\data\\export1.docx", "E:\\data\\export1.pdf");
//        WordUtil.convertWordToPdf("E:\\data\\反担保保证合同（非融）--法人.docx", "E:\\data\\反担保保证合同（非融）--法人.pdf");
//        WordUtil.convertWordToPdf("E:\\data\\代出保函再担保.doc", "E:\\data\\代出保函再担保.pdf");
    }
}
