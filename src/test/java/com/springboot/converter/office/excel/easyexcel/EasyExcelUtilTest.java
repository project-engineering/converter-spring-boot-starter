package com.springboot.converter.office.excel.easyexcel;

import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.springboot.converter.entity.User;
import com.springboot.converter.office.excel.easyexcel.listener.EasyExcelDefaultListener;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ResourceLoader;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class EasyExcelUtilTest {
    @Resource
    private ResourceLoader resourceLoader;
    private List<User> genData() {
        List<User> data = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            User user = new User();
            user.setName("张三"+i);
            user.setAge(17 + i);
            user.setSex("1");
            user.setCity("安庆");
            user.setTel("13812345678");
            user.setAvatar("https://www.baidu.com/img/bd_logo1.png");
            data.add(user);
        }
        return data;
    }

    private String file = "E://aa.xlsx";

    @Test
    public void syncRead1() {
        final List<Map<Integer, String>> list = ExcelUtil.syncRead(file);
        list.forEach(map -> {
            map.forEach((k, v) -> System.out.println(k + " - " + v));
        });
    }

    @Test
    public void syncRead2() {
        final List<Map<Integer, String>> list = ExcelUtil.syncRead(file, 1);
        list.forEach(map -> {
            map.forEach((k, v) -> System.out.println(k + " - " + v));
        });
    }

    @Test
    public void syncRead3() {
        final List<Map<Integer, String>> list = ExcelUtil.syncRead(file, 2, 0);
        list.forEach(map -> {
            map.forEach((k, v) -> System.out.println(k + " - " + v));
        });
    }

    @Test
    public void syncRead4() throws FileNotFoundException {
        final FileInputStream is = new FileInputStream(file);
        final List<Map<Integer, String>> list = ExcelUtil.syncRead(is, 2, 0);
        list.forEach(map -> {
            map.forEach((k, v) -> System.out.println(k + " - " + v));
        });
    }

    @Test
    public void syncRead5() throws FileNotFoundException {
        final List<Map<Integer, String>> list = ExcelUtil.syncRead(file, 0);
        list.forEach(map -> {
            map.forEach((k, v) -> System.out.println(k + " - " + v));
        });
    }


    @Test
    public void syncReadModel1() {
        final List<User> list = ExcelUtil.syncReadModel(file, User.class);
        list.forEach(System.out::println);
    }

    @Test
    public void syncReadModel2() {
        final List<User> list = ExcelUtil.syncReadModel(file, User.class, 2);
        list.forEach(System.out::println);
    }

    @Test
    public void syncReadModel3() throws FileNotFoundException {
        final List<User> list = ExcelUtil.syncReadModel(new FileInputStream(file), User.class, 2);
        list.forEach(System.out::println);
    }

    @Test
    public void syncReadModel4() {
        final List<User> list = ExcelUtil.syncReadModel(new File(file), User.class, 2);
        list.forEach(System.out::println);
    }

    @Test
    public void asyncRead1() {
        ExcelUtil.asyncRead(file, new EasyExcelDefaultListener<Map<Integer, String>>() {
            @Override
            protected void fun(List<Map<Integer, String>> list) {
                list.forEach(System.out::println);
            }
        });
    }

    @Test
    public void asyncRead2() {
        ExcelUtil.asyncRead(file, new EasyExcelDefaultListener<Map<Integer, String>>() {
            @Override
            protected void fun(List<Map<Integer, String>> list) {
                list.forEach(System.out::println);
            }
        }, 1);
    }

    @Test
    public void asyncRead3() {
        ExcelUtil.asyncRead(file, new EasyExcelDefaultListener<Map<Integer, String>>() {
            @Override
            protected void fun(List<Map<Integer, String>> list) {
                list.forEach(System.out::println);
            }
        }, 1, 0);
    }


    @Test
    public void asyncReadModel1() {
        ExcelUtil.asyncReadModel(file, new EasyExcelDefaultListener<User>() {
            @Override
            protected void fun(List<User> list) {
                list.forEach(System.out::println);
            }
        }, User.class);
    }

    @Test
    public void asyncReadModel2() {
        ExcelUtil.asyncReadModel(file, new EasyExcelDefaultListener<User>() {
            @Override
            protected void fun(List<User> list) {
                list.forEach(System.out::println);
            }
        }, User.class, 1);
    }

    @Test
    public void asyncReadModel3() {
        ExcelUtil.asyncReadModel(new File(file), new EasyExcelDefaultListener<User>() {
            @Override
            protected void fun(List<User> list) {
                list.forEach(System.out::println);
            }
        }, User.class, 1);
    }

    @Test
    public void asyncReadModel4() throws FileNotFoundException {
        ExcelUtil.asyncReadModel(new FileInputStream(file), new EasyExcelDefaultListener<User>() {
            @Override
            protected void fun(List<User> list) {
                list.forEach(System.out::println);
            }
        }, User.class, 1);
    }

    @Test
    public void write1() {
        List<List<String>> head = List.of(
                List.of("id"),
                List.of("name"),
                List.of("age")
        );
        List<List<Object>> data = List.of(
                List.of(1001, "aaaa", 11),
                List.of(1002, "aaaa", 22),
                List.of(1003, "cccc", 33),
                List.of(1004, "dddd", 44),
                List.of(1005, "eeee", 55)
        );

        ExcelUtil.write("E://dd.xlsx", head, data);
    }

    @Test
    public void write2() {
        List<List<String>> head = List.of(
                List.of("id"),
                List.of("name"),
                List.of("age")
        );
        List<List<Object>> data = List.of(
                List.of(1001, "aaaa", 11),
                List.of(1002, "bbbb", 22),
                List.of(1003, "cccc", 33),
                List.of(1004, "dddd", 44),
                List.of(1005, "eeee", 55)
        );
        ExcelUtil.write("E://dd.xlsx", head, data, 0, "test");
    }

    @Test
    public void write4() {
        ExcelUtil excelUtil = new ExcelUtil() {
            @Override
            protected int getTotal(Map<String, Object> param) {
                return 100000;
            }

            @Override
            protected List<?> getPageData(Map<String, Object> param) {
                List<Object> data = new ArrayList<>();
                for (int i = 0; i < 100000; i++) {
                    User user = new User();
                    user.setName("张三");
                    user.setAge(17 + i);
                    user.setSex("1");
                    user.setCity("安庆");
                    user.setTel("13812345678");
                    user.setAvatar("https://www.baidu.com/img/bd_logo1.png");
                    data.add(user);
                }
                List<Object> result = paginate(data, (Integer) param.get("pageSize"), (Integer) param.get("pageNo"));
                return result;
            }
        };
        // 分页查询
        // 下载的条件
        Map<String, Object> param = new HashMap<>();
        param.put("pageNo", 1);
        param.put("pageSize", 10000);
        //需要合并的列
        int[] mergeColumnIndex = {0,2, 3};

        //设置第几行开始合并
        int mergeRowIndex = 1;
        excelUtil.write("E://data","用户信息","用户信息",param,User.class, ExcelTypeEnum.XLSX.getValue(),10000,mergeRowIndex,mergeColumnIndex);
    }

    public static <T> List<T> paginate(List<T> list, int pageSize, int page) {
        return list.stream()
                .skip((page - 1) * pageSize) // 跳过前面的元素
                .limit(pageSize) // 取指定数量的元素
                .collect(Collectors.toList()); // 转换为List
    }


    @Test
    public void writeTemplate1() {
        List<User> data = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            User user = new User();
            user.setName("张三"+i);
            user.setAge(17 + i);
            user.setSex("1");
            user.setCity("安庆");
            user.setTel("13812345678");
            user.setAvatar("https://www.baidu.com/img/bd_logo1.png");
            data.add(user);
        }
        ExcelUtil.writeTemplate("E://dd.xlsx", "E://bb.xlsx", data);
    }

    @Test
    public void writeTemplate2() {
        List<Object> data = new ArrayList<>();
//        for (int i = 0; i < 100; i++) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("name", "张三"+i);
//            map.put("age", 17 + i);
//            map.put("sex", "1");
//            map.put("city", "安庆");
//            map.put("tel", "13812345678");
//            map.put("avatar", "https://www.baidu.com/img/bd_logo1.png");
//            data.add(map);
//        }
        for (int i = 0; i < 100; i++) {
            User user = new User();
            user.setName("张三"+i);
            user.setAge(17 + i);
            user.setSex("1");
            user.setCity("安庆");
            user.setTel("13812345678");
            user.setAvatar("https://www.baidu.com/img/bd_logo1.png");
            data.add(user);
        }
        Map<String,Object> map = new HashMap<>();
        map.put("content","用户信息");
        ExcelUtil.writeTemplate("E://ccc.xlsx", "E://bb.xlsx",data, map);
    }

    @Test
    public void writeTemplate3() {
        List<Object> data = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            User user = new User();
            user.setName("张三"+i);
            user.setAge(17 + i);
            user.setSex("1");
            user.setCity("安庆");
            user.setTel("13812345678");
            user.setAvatar("https://www.baidu.com/img/bd_logo1.png");
            data.add(user);
        }
        ExcelUtil.writeTemplate("E://ccc.xlsx", "E://bbb.xlsx",genData(), User.class);
    }

    @Test
    public void write20() {
        ExcelUtil.write(file, User.class, genData());
    }

    @Test
    public void fun21() {
        ExcelUtil.write(file, User.class, genData(), 0, "用户");
    }

    @Test
    public void fun22() {
        ExcelUtil.write(file, User.class, genData(), new CellWriteHandler() {
            //…… 这个方法还没有遇到应用场景
        }, 0, "用户");
    }


    @Test
    public void writeInclude() {
        ExcelUtil.writeInclude(file, User.class, genData(), Set.of("id", "nickname", "birth"), 1, "test");
    }

    @Test
    public void writeExclude() {
        ExcelUtil.writeExclude(file, User.class, genData(), Set.of("id", "nickname", "birth"), 1, "test");
    }
    //
    @Test
    public void writeWithSheets1() {
        final EasyExcelWriteTool easyExcelWriteTool = ExcelUtil.writeWithSheets(file)
                .writeModel(User.class, genData(), "user1")
                .writeModel(User.class, genData(), "user2")
                .writeModel(User.class, genData(), "user3");
        easyExcelWriteTool.finish();
    }
    @Test
    public void writeWithSheets2() {
        final EasyExcelWriteTool easyExcelWriteTool = ExcelUtil.writeWithSheets(new File(file))
                .writeModel(User.class, genData(), "user2");
        easyExcelWriteTool.finish();
    }
    @Test
    public void writeWithSheets3() throws FileNotFoundException {
        final EasyExcelWriteTool easyExcelWriteTool = ExcelUtil.writeWithSheets(new FileOutputStream(file))
                .writeModel(User.class, genData(), "user2");
        easyExcelWriteTool.finish();
    }
}