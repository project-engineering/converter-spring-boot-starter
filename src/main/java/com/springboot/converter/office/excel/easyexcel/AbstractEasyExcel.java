package com.springboot.converter.office.excel.easyexcel;

import java.util.List;
import java.util.Map;

public abstract class AbstractEasyExcel {
    /**
     * 获取总数据
     *
     * @param param 条件参数
     * @return
     */
    protected abstract int getTotal(Map<String, Object> param);

    /**
     * 获取分页数据
     *
     * @param param 条件参数，包含分页参数
     * @return
     */
    protected abstract List<?> getPageData(Map<String, Object> param);
}
