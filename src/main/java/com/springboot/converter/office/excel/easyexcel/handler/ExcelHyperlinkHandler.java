package com.springboot.converter.office.excel.easyexcel.handler;

import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;

import java.util.List;

/**
 * 动态添加超链接
 */
public class ExcelHyperlinkHandler implements CellWriteHandler {
    /**
     * 添加超链接的字段的下标
     */
    private int[] mergeColumnIndex;
    /**
     * 从第几行开始添加，0代表标题行
     */
    private int mergeRowIndex;

    public ExcelHyperlinkHandler() {
    }

    public ExcelHyperlinkHandler(int mergeRowIndex, int[] mergeColumnIndex) {
        this.mergeRowIndex = mergeRowIndex;
        this.mergeColumnIndex = mergeColumnIndex;
    }

    @Override
    public void beforeCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row,
                                 Head head, Integer integer, Integer integer1, Boolean aBoolean) {

    }

    @Override
    public void afterCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Cell cell,
                                Head head, Integer integer, Boolean aBoolean) {

    }

    @Override
    public void afterCellDataConverted(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, WriteCellData<?> cellData, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {

    }

    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, List<WriteCellData<?>> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {
        //当前行
        int curRowIndex = cell.getRowIndex();
        //当前列
        int curColIndex = cell.getColumnIndex();

        if (curRowIndex >= mergeRowIndex) {
            for (int i = 0; i < mergeColumnIndex.length; i++) {
                if (curColIndex == mergeColumnIndex[i]) {
                    mergeWithPrevRow(writeSheetHolder, cell, curRowIndex, curColIndex);
                    break;
                }
            }
        }
    }

    private void mergeWithPrevRow(WriteSheetHolder writeSheetHolder, Cell cell, int curRowIndex, int curColIndex) {
        int sheetNo = writeSheetHolder.getSheetNo();
        if (0 == sheetNo) {
            Row row =  writeSheetHolder.getSheet().getRow(curRowIndex);
            Cell prevCell = row.getCell(1);
            String prevCellValue = prevCell.getStringCellValue();
            //获得超链接，当前单元格的内容就是一个超链接
            if (StrUtil.isNotBlank(prevCellValue)) {
                CreationHelper createHelper = writeSheetHolder.getSheet().getWorkbook().getCreationHelper();
                //创建超链接
                //HyperlinkType.DOCUMENT 表示文档中的位置
                Hyperlink hyperlink = createHelper.createHyperlink(HyperlinkType.DOCUMENT);
                //设置超链接的地址
                hyperlink.setAddress("'"+prevCellValue+"'!A1");
                //添加超链接
                cell.setHyperlink(hyperlink);
            }
        }else {
            Row row =  writeSheetHolder.getSheet().getRow(curRowIndex);
            Cell prevCell = row.getCell(0);
            String prevCellValue = prevCell.getStringCellValue();
            //获得超链接，当前单元格的内容就是一个超链接
            if (StrUtil.isNotBlank(prevCellValue)) {
                CreationHelper createHelper = writeSheetHolder.getSheet().getWorkbook().getCreationHelper();
                //创建超链接
                //HyperlinkType.DOCUMENT 表示文档中的位置
                Hyperlink hyperlink = createHelper.createHyperlink(HyperlinkType.DOCUMENT);
                //设置超链接的地址
                hyperlink.setAddress("'目录'!A1");
                //添加超链接
                cell.setHyperlink(hyperlink);
            }
        }

    }
}
