package com.springboot.converter.office.excel.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import org.apache.poi.ss.formula.functions.T;

import java.io.File;
import java.io.OutputStream;
import java.util.List;

public class EasyExcelWriteTool {
    private int sheetNum;
    private ExcelWriter excelWriter;

    public EasyExcelWriteTool(OutputStream outputStream) {
        excelWriter = EasyExcel.write(outputStream).build();
    }

    public EasyExcelWriteTool(File file) {
        excelWriter = EasyExcel.write(file).build();
    }

    public EasyExcelWriteTool(String filePath) {
        excelWriter = EasyExcel.write(filePath).build();
    }

    /**
     * 链式模板表头写入
     * @param clazz 表头格式
     * @param data 数据 List<ExcelModel> 或者List<List<Object>>
     * @return
     */
    public <T> EasyExcelWriteTool writeModel(Class clazz, List<T> data, String sheetName) {
        final WriteSheet writeSheet = EasyExcel.writerSheet(this.sheetNum++, sheetName).head(clazz).build();
        excelWriter.write(data, writeSheet);
        return this;
    }

    /**
     * 链式自定义表头写入
     * @param head
     * @param data 数据 List<ExcelModel> 或者List<List<Object>>
     * @param sheetName
     * @return
     */
    public EasyExcelWriteTool write(List<List<String>> head, List<T> data, String sheetName) {
        final WriteSheet writeSheet = EasyExcel.writerSheet(this.sheetNum++, sheetName).head(head).build();
        excelWriter.write(data, writeSheet);
        return this;
    }

    /**
     * 使用此类结束后，一定要关闭流
     */
    public void finish() {
        excelWriter.finish();
    }
}