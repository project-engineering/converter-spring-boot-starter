package com.springboot.converter.office.excel.easyexcel.listener;

import cn.hutool.json.JSONUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelDataConvertException;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 默认的监听器，处理excel数据，并保存到数据库中
 */
@Log4j2
public abstract class EasyExcelDefaultListener<T> extends AnalysisEventListener<T> {

    /**
     * 批处理阈值
     */
    private static final int BATCH_COUNT = 20;

    /**
     * 用来存放待处理的数据
     */
    @Getter
    private List<T> list = new ArrayList<>(BATCH_COUNT);

    /**
     * 读取excel数据前操作 <br>
     *
     * 只有不读取表头数据时才会触发此方法)
     */
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        log.info("======================================================");
        log.info("解析第一行数据:{}" , JSONUtil.toJsonStr(headMap));
        log.info("======================================================");
    }

    /**
     * 读取excel数据操作
     * @param obj
     * @param context
     */
    @Override
    public void invoke(T obj, AnalysisContext context) {
        list.add(obj);

        if (list.size() >= BATCH_COUNT) {
            //将数据保存到数据库中
            fun(list);
            list.clear();
        }
    }

    /**
     * 具体业务
     */
    protected abstract void fun(List<T> list);

    /**
     * 读取完excel数据后的操作
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        if (list.size() > 0) {
            fun(list);
        }
    }

    /**
     * 在读取excel异常 获取其他异常下会调用本接口。抛出异常则停止读取。如果这里不抛出异常则 继续读取下一行。
     */
    @Override
    public void onException(Exception exception, AnalysisContext context) {
        log.error("解析失败，但是继续解析下一行:{}", exception.getMessage());
        if (exception instanceof ExcelDataConvertException) {
            ExcelDataConvertException ex = (ExcelDataConvertException) exception;
            log.error("第{}行，第{}列解析异常，数据为:{}", ex.getRowIndex(), ex.getColumnIndex(), ex.getCellData());
        }
    }
}