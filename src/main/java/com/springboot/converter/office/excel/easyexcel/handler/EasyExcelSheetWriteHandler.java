package com.springboot.converter.office.excel.easyexcel.handler;

import com.alibaba.excel.write.handler.SheetWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteWorkbookHolder;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * easyExcel 写sheet 处理器
 */
public class EasyExcelSheetWriteHandler implements SheetWriteHandler {

    private String title;
    @Override
    public void beforeSheetCreate(WriteWorkbookHolder writeWorkbookHolder, WriteSheetHolder writeSheetHolder) {

    }

    public EasyExcelSheetWriteHandler() {
        super();
    }

    public EasyExcelSheetWriteHandler(String title) {
        this.title = title;
    }

    @Override
    public void afterSheetCreate(WriteWorkbookHolder writeWorkbookHolder, WriteSheetHolder writeSheetHolder) {
        Workbook workbook = writeWorkbookHolder.getWorkbook();
        Sheet sheet = workbook.getSheetAt(0);
        Row row1 = sheet.createRow(0);
        row1.setHeight((short) 800);
        Cell cell = row1.createCell(0);
        //设置标题
        cell.setCellValue(title);
        CellStyle cellStyle = workbook.createCellStyle();
        //设置标题样式 字体加粗，字体大小400，字体为宋体，水平居中，垂直居中
        //垂直居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        //水平居中
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        Font font = workbook.createFont();
        //设置字体加粗
        font.setBold(true);
        //设置字体大小
        font.setFontHeight((short) 400);
        //设置字体为宋体
        font.setFontName("宋体");
        cellStyle.setFont(font);
        //设置自动换行
        cellStyle.setWrapText(true);
        cell.setCellStyle(cellStyle);
        sheet.addMergedRegionUnsafe(new CellRangeAddress(0, 0, 0, 9));

        //设置将所有列调整为一页
        sheet.setFitToPage(true);
        //设置打印内容水平居中显示
        sheet.setHorizontallyCenter(true);
        //设置打印页面边距
        sheet.setMargin(Sheet.TopMargin, 0.3);
        sheet.setMargin(Sheet.BottomMargin, 0.5);
        sheet.setMargin(Sheet.LeftMargin, 0.3);
        sheet.setMargin(Sheet.RightMargin, 0.3);
        //打印设置对象
        PrintSetup print = sheet.getPrintSetup();
        //并缩减打印输出只有一页宽
        print.setFitHeight((short)0);
        //设置竖屏打印（false），横屏打印（true）
        print.setLandscape(false);
        //设置A4纸打印
        print.setPaperSize(PrintSetup.A4_PAPERSIZE);
    }
}
