package com.springboot.converter.office.excel.easyexcel;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.handler.WriteHandler;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import com.springboot.converter.UtilValidate;
import com.springboot.converter.office.excel.easyexcel.handler.*;
import com.springboot.converter.util.DateUtil;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StopWatch;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * EasyExcel工具类
 */
@Log4j2
public abstract class ExcelUtil extends AbstractEasyExcel {

    //====================================================无JAVA模型读取excel数据===============================================================

    /**
     * 同步无模型读（默认读取sheet0,从第2行开始读）
     * @param file excel文件的绝对路径
     */
    public static List<Map<Integer, String>> syncRead(String file) {
        return EasyExcelFactory.read(file).sheet().doReadSync();
    }

    /**
     * 同步无模型读（自定义读取sheetX，从第2行开始读）
     * @param file excel文件的绝对路径
     * @param sheetNo sheet页号，从0开始
     */
    public static List<Map<Integer, String>> syncRead(String file, Integer sheetNo) {
        return EasyExcelFactory.read(file).sheet(sheetNo).doReadSync();
    }

    /**
     * 同步无模型读（指定sheet和表头占的行数）
     * @param file
     * @param sheetNo sheet页号，从0开始
     * @param headNo 表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static List<Map<Integer, String>> syncRead(String file, Integer sheetNo, Integer headNo) {
        return EasyExcelFactory.read(file).sheet(sheetNo).headRowNumber(headNo).doReadSync();
    }

    /**
     * 同步无模型读（指定sheet和表头占的行数）
     * @param inputStream
     * @param sheetNo sheet页号，从0开始
     * @param headNo 表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static List<Map<Integer, String>> syncRead(InputStream inputStream, Integer sheetNo, Integer headNo) {
        return EasyExcelFactory.read(inputStream).sheet(sheetNo).headRowNumber(headNo).doReadSync();
    }

    /**
     * 同步无模型读（指定sheet和表头占的行数）
     * @param file
     * @param sheetNo sheet页号，从0开始
     * @param headNo 表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static List<Map<Integer, String>> syncRead(File file, Integer sheetNo, Integer headNo) {
        return EasyExcelFactory.read(file).sheet(sheetNo).headRowNumber(headNo).doReadSync();
    }
    //====================================================无JAVA模型读取excel数据===============================================================

    //====================================================将excel数据同步到JAVA模型属性里===============================================================

    /**
     * 同步按模型读（默认读取sheet0,不读取表头，从第2行开始读）
     * @param file
     * @param clazz 模型的类类型（excel数据会按该类型转换成对象）
     */
    public static <T> List<T> syncReadModel(String file, Class clazz) {
        return EasyExcelFactory.read(file).sheet().head(clazz).doReadSync();
    }

    /**
     * 同步按模型读（默认表头占一行，不读取表头，从第2行开始读）
     * @param file
     * @param clazz 模型的类类型（excel数据会按该类型转换成对象）
     * @param sheetNo sheet页号，从0开始
     */
    public static <T> List<T> syncReadModel(String file, Class clazz, Integer sheetNo) {
        return EasyExcelFactory.read(file).sheet(sheetNo).head(clazz).doReadSync();
    }

    /**
     * 同步按模型读（指定sheet,不读取表头）
     * @param inputStream
     * @param clazz 模型的类类型（excel数据会按该类型转换成对象）
     * @param sheetNo sheet页号，从0开始
     */
    public static <T> List<T> syncReadModel(InputStream inputStream, Class clazz, Integer sheetNo) {
        return EasyExcelFactory.read(inputStream).sheet(sheetNo).head(clazz).doReadSync();
    }

    /**
     * 同步按模型读（指定sheet,不读取表头）
     * @param file
     * @param clazz 模型的类类型（excel数据会按该类型转换成对象）
     * @param sheetNo sheet页号，从0开始
     */
    public static <T> List<T> syncReadModel(File file, Class clazz, Integer sheetNo) {
        return EasyExcelFactory.read(file).sheet(sheetNo).head(clazz).doReadSync();
    }
    //====================================================将excel数据同步到JAVA模型属性里===============================================================

    //====================================================异步读取excel数据===============================================================

    /**
     * 异步无模型读（默认读取sheet0,不读取表头,从第2行开始读）
     * @param listener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param file 表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static <T> void asyncRead(String file, AnalysisEventListener<T> listener) {
        EasyExcelFactory.read(file, listener).sheet().doRead();
    }

    /**
     * 异步无模型读（默认表头占一行，不读取表头,从第2行开始读）
     * @param file 表头占的行数，从0开始（如果要连表头一起读出来则传0）
     * @param listener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param sheetNo sheet页号，从0开始
     */
    public static <T> void asyncRead(String file, AnalysisEventListener<T> listener, Integer sheetNo) {
        EasyExcelFactory.read(file, listener).sheet(sheetNo).doRead();
    }

    /**
     * 异步无模型读（指定sheet和表头占的行数）
     * @param inputStream
     * @param listener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param sheetNo sheet页号，从0开始
     * @param headNo 表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static <T> void asyncRead(InputStream inputStream, AnalysisEventListener<T> listener, Integer sheetNo, Integer headNo) {
        EasyExcelFactory.read(inputStream, listener).sheet(sheetNo).headRowNumber(headNo).doRead();
    }

    /**
     * 异步无模型读（指定sheet和表头占的行数）
     * @param file
     * @param listener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param sheetNo sheet页号，从0开始
     * @param headNo 表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static <T> void asyncRead(File file, AnalysisEventListener<T> listener, Integer sheetNo, Integer headNo) {
        EasyExcelFactory.read(file, listener).sheet(sheetNo).headRowNumber(headNo).doRead();
    }

    /**
     * 异步无模型读（指定sheet和表头占的行数）
     * @param file
     * @param listener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param sheetNo sheet页号，从0开始
     * @param headNo 表头占的行数，从0开始（如果要连表头一起读出来则传0）
     * @return
     */
    public static <T> void asyncRead(String file, AnalysisEventListener<T> listener, Integer sheetNo, Integer headNo) {
        EasyExcelFactory.read(file, listener).sheet(sheetNo).headRowNumber(headNo).doRead();
    }
    //====================================================异步读取excel数据===============================================================

    //====================================================将excel数据异步到JAVA模型属性里===============================================================
    /**
     * 异步按模型读取（默认读取sheet0,不读取表头,从第2行开始读）
     * @param file
     * @param listener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param clazz 模型的类类型（excel数据会按该类型转换成对象）
     */
    public static <T> void asyncReadModel(String file, AnalysisEventListener<T> listener, Class clazz) {
        EasyExcelFactory.read(file, clazz, listener).sheet().doRead();
    }

    /**
     * 异步按模型读取（默认表头占一行，不读取表头,从第2行开始读）
     * @param file
     * @param listener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param clazz 模型的类类型（excel数据会按该类型转换成对象）
     * @param sheetNo  sheet页号，从0开始
     */
    public static <T> void asyncReadModel(String file, AnalysisEventListener<T> listener, Class clazz, Integer sheetNo) {
        EasyExcelFactory.read(file, clazz, listener).sheet(sheetNo).doRead();
    }

    /**
     * 异步按模型读取
     * @param file
     * @param listener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param clazz 模型的类类型（excel数据会按该类型转换成对象）
     * @param sheetNo  sheet页号，从0开始
     */
    public static <T> void asyncReadModel(File file, AnalysisEventListener<T> listener, Class clazz, Integer sheetNo) {
        EasyExcelFactory.read(file, clazz, listener).sheet(sheetNo).doRead();
    }

    /**
     * 异步按模型读取
     * @param inputStream
     * @param listener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param clazz 模型的类类型（excel数据会按该类型转换成对象）
     * @param sheetNo  sheet页号，从0开始
     */
    public static <T> void asyncReadModel(InputStream inputStream, AnalysisEventListener<T> listener, Class clazz, Integer sheetNo) {
        EasyExcelFactory.read(inputStream, clazz, listener).sheet(sheetNo).doRead();
    }
    //====================================================将excel数据异步到JAVA模型属性里===============================================================


    //====================================================无JAVA模型写文件===============================================================
    /**
     * 无模板写文件
     * @param file
     * @param head 表头数据
     * @param data 表内容数据
     */
    public static void write(String file, List<List<String>> head, List<List<Object>> data) {
        EasyExcel.write(file)
                .head(head)
                .sheet()
                // 自定义宽度策略
                .registerWriteHandler(new CustomWidthStyleStrategy())
                .doWrite(data);
    }

    /**
     * 无模板写文件
     * @param file
     * @param head 表头数据
     * @param data 表内容数据
     * @param sheetNo sheet页号，从0开始
     * @param sheetName sheet名称
     */
    public static void write(String file, List<List<String>> head, List<List<Object>> data, Integer sheetNo, String sheetName) {
        EasyExcel.write(file).head(head).sheet(sheetNo, sheetName).doWrite(data);
    }

    /**
     * 无模板写文件
     * @param filePath
     * @param fileNamePrefix
     * @param sheetName
     * @param param
     * @param clazz
     * @param fileType
     * @param pageSize
     * @param mergeRowIndex
     * @param mergeColumnIndex
     * @param <T>
     */
    public <T> void write(String filePath, String fileNamePrefix,String sheetName,Map<String, Object> param, Class<T> clazz,String fileType,int pageSize,int mergeRowIndex,int[] mergeColumnIndex) {
        log.info("开始导出Excel数据，文件路径：{}",filePath);
        StopWatch swatch = new StopWatch();
        swatch.start();
        // 获取当前时间作为文件名后缀
        String currentTime = DateUtil.covertObjToString(DateUtil.getCurrLocalDateTime(), DateUtil.DATE_FORMATTER_11);
        if (UtilValidate.isEmpty(fileType)) {
            fileType = ExcelTypeEnum.XLSX.getValue();
        }
        // 组装文件名
        String fileName = fileNamePrefix + currentTime + fileType;

        // 创建文件夹
        FileUtil.mkdir(filePath);

        // 创建ExcelWriter并注册写入处理器
        ExcelWriter excelWriter;
        if (UtilValidate.isNotEmpty(mergeRowIndex)&&UtilValidate.isNotEmpty(mergeColumnIndex)) {
            excelWriter = EasyExcel.write(addTrailingSlash(filePath) + fileName,clazz)
                    .registerWriteHandler(new CustomWidthStyleStrategy())
                    .registerWriteHandler(new CustomCellStyleStrategy())
                    .registerWriteHandler(new ExcelFillCellColumnMergeStrategy(mergeRowIndex, mergeColumnIndex))
                    .build();
        } else {
            excelWriter = EasyExcel.write(addTrailingSlash(filePath) + fileName,clazz)
                    .registerWriteHandler(new CustomWidthStyleStrategy())
                    .registerWriteHandler(new CustomCellStyleStrategy())
                    .build();
        }

        try {
            // 获取总数
            int total = getTotal(param);
            int totalPages = (total + pageSize - 1) / pageSize;
            //总数超过pageSize条，就分多个sheet页
            WriteSheet writeSheet ;
            if (total > pageSize) {
                for (int i = 0; i < totalPages; i++) {
                    // 获取分页数据
                    param.put("pageNo", i+1);
                    param.put("pageSize", pageSize);
                    List<?> pageDataList = getPageData(param);
                    // 填充数据
                    StopWatch stopWatch = new StopWatch();
                    stopWatch.start();
                    writeSheet = EasyExcel.writerSheet().sheetName(sheetName+(i+1)).build();
                    excelWriter.write(pageDataList, writeSheet);
                    stopWatch.stop();
                    log.info("填充数据耗时：{} ms", stopWatch.getTotalTimeMillis());
                }
            } else {
                // 这里注意 如果同一个sheet只要创建一次
                writeSheet = EasyExcel.writerSheet().sheetName(sheetName).build();
                for (int i = 0; i < totalPages; i++) {
                    // 获取分页数据
                    param.put("pageNo", i+1);
                    param.put("pageSize", pageSize);
                    List<?> pageDataList = getPageData(param);
                    // 填充数据
                    StopWatch stopWatch = new StopWatch();
                    stopWatch.start();
                    excelWriter.write(pageDataList, writeSheet);
                    stopWatch.stop();
                    log.info("填充数据耗时：{} ms", stopWatch.getTotalTimeMillis());
                }
            }
        } catch (Exception e) {
            log.error("下载Excel数据失败，失败原因：{}", e.getMessage());
            throw new RuntimeException("下载Excel数据失败，失败原因：" + e.getMessage());
        } finally {
            // 关闭ExcelWriter
            if (excelWriter != null) {
                excelWriter.finish();
            }
        }
        swatch.stop();
        log.info("导出成功,文件：{},总耗时：{} ms",filePath+fileName,swatch.getTotalTimeMillis());
    }
    //====================================================无JAVA模型写文件===============================================================

    //====================================================有Excel模板写文件===============================================================
    /**
     * 根据excel模板文件写入文件，可以实现向已有文件中添加数据的功能
     * @param file
     * @param template
     * @param data
     */
    public static <T> void writeTemplate(String file, String template, List<T> data) {
        EasyExcel.write(file).withTemplate(template).sheet().doWrite(data);
    }

    /**
     * 根据excel模板文件写入文件，可以实现向已有文件中添加数据的功能
     * @param file 输出的文件路径+文件名 例如：E://dd.xlsx
     * @param template 模板 文件路径+文件名 例如：E://template.xlsx
     * @param dataList 导出的数据列表
     * @param exportMap 导出的数据map
     * @param <T>
     */
    public static <T> void writeTemplate(String file, String template, List<T> dataList,Map<String,Object> exportMap) {
        ExcelWriter excelWriter = EasyExcel.write(file).withTemplate(template)
                .registerWriteHandler(new CustomWidthStyleStrategy())
                .registerWriteHandler(new CustomCellStyleStrategy())
                .build();
        WriteSheet writeSheet = EasyExcel.writerSheet().build();
        if (CollUtil.isEmpty(dataList)&&CollUtil.isEmpty(exportMap)) {
            throw new IllegalArgumentException("dataList or exportMap is empty");
        }
        if (CollUtil.isNotEmpty(exportMap)) {
            excelWriter.fill(exportMap,writeSheet);
        }
        if (CollUtil.isNotEmpty(dataList)) {
            excelWriter.fill(dataList,writeSheet);
        }
        excelWriter.finish();
        log.info("导出成功,文件：{}",file);
    }

    /**
     * 根据excel模板文件写入文件
     * @param file
     * @param template
     * @param clazz
     * @param data
     */
    public static <T> void writeTemplate(String file, String template, List<T> data, Class clazz) {
        EasyExcel.write(file, clazz)
                .withTemplate(template)
                .sheet()
                //设置列宽策略
                .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy())
                .registerWriteHandler(new CustomCellStyleStrategy())
                .doWrite(data);
    }


    /**
     * 根据excel模板文件写入文件并导出到浏览器
     * @param response 响应对象
     * @param fileName 导出的文件名称
     * @param template 模板文件路径+文件名 例如：E://template.xlsx
     * @param data 导出的数据列表
     * @param clazz 使用的模板类
     */
    public static <T> void writeTemplate(HttpServletResponse response,String fileName, String template, List<T> data, Class<T> clazz){
        try {
            setResponse(response, fileName);
            EasyExcel.write(response.getOutputStream(), clazz)
                    .withTemplate(template)
                    .sheet()
                    //设置列宽策略
                    .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy())
                    .registerWriteHandler(new CustomCellStyleStrategy())
                    .doWrite(data);
        } catch (IOException e) {
            log.error("导出excel失败,原因：{}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * 根据模板的路径获取模板文件对象
     * classpath根目录下：/templates/report.xlsx
     */
    private static File getTemplateFile(String template) throws IOException {
        Resource resource =new ClassPathResource("/templates/"+template);
        return  resource.getFile();
    }

    /**
     *
     * @param path
     * @return
     */
    public static String addTrailingSlash(String path) {
        if (path == null) {
            path = "";
        }
        if (!path.endsWith("/")) {
            path += "/";
        }
        return path;
    }


    //====================================================有模板写文件===============================================================

    //====================================================无模板写文件===============================================================
    /**
     * 按模板写文件
     * @param file
     * @param clazz 表头模板
     * @param data 数据
     */
    public static <T> void write(String file, Class clazz, List<T> data) {
        EasyExcel.write(file, clazz)
                .sheet()
                // 自定义宽度策略
                .registerWriteHandler(new CustomWidthStyleStrategy())
                .registerWriteHandler(new CustomCellStyleStrategy())
                .doWrite(data);
    }

    /**
     * 按模板写文件
     * @param file
     * @param clazz 表头模板
     * @param data 数据
     * @param sheetNo sheet页号，从0开始
     * @param sheetName sheet名称
     */
    public static <T> void write(String file, Class clazz, List<T> data, Integer sheetNo, String sheetName) {
        EasyExcel.write(file, clazz).sheet(sheetNo, sheetName).doWrite(data);
    }

    /**
     * 按模板写文件
     * @param file
     * @param clazz 表头模板
     * @param data 数据
     * @param writeHandler 自定义的处理器，比如设置table样式，设置超链接、单元格下拉框等等功能都可以通过这个实现（需要注册多个则自己通过链式去调用）
     * @param sheetNo sheet页号，从0开始
     * @param sheetName sheet名称
     */
    public static <T> void write(String file, Class clazz, List<T> data, WriteHandler writeHandler, Integer sheetNo, String sheetName) {
        EasyExcel.write(file, clazz).registerWriteHandler(writeHandler).sheet(sheetNo, sheetName).doWrite(data);
    }

    /**
     * 按模板写文件（包含某些字段）
     * @param file
     * @param clazz 表头模板
     * @param data 数据
     * @param includeCols 包含字段集合，根据字段名称显示
     * @param sheetNo sheet页号，从0开始
     * @param sheetName sheet名称
     */
    public static <T> void writeInclude(String file, Class clazz, List<T> data, Set<String> includeCols, Integer sheetNo, String sheetName) {
        EasyExcel.write(file, clazz).includeColumnFieldNames(includeCols).sheet(sheetNo, sheetName).doWrite(data);
    }

    /**
     * 按模板写文件（排除某些字段）
     * @param file
     * @param clazz 表头模板
     * @param data 数据
     * @param excludeCols 过滤排除的字段，根据字段名称过滤
     * @param sheetNo sheet页号，从0开始
     * @param sheetName sheet名称
     */
    public static <T> void writeExclude(String file, Class clazz, List<T> data, Set<String> excludeCols, Integer sheetNo, String sheetName) {
        EasyExcel.write(file, clazz).excludeColumnFieldNames(excludeCols).sheet(sheetNo, sheetName).doWrite(data);
    }

    //------------------------------------------------------------------------------------------------
    /**
     * 多个sheet页的数据链式写入
     *
     * @param file
     */
    public static EasyExcelWriteTool writeWithSheets(String file) {
        return new EasyExcelWriteTool(file);
    }

    /**
     * 多个sheet页的数据链式写入
     *
     * @param file
     */
    public static EasyExcelWriteTool writeWithSheets(File file) {
        return new EasyExcelWriteTool(file);
    }

    /**
     * 多个sheet页的数据链式写入
     *
     * @param outputStream
     */
    public static EasyExcelWriteTool writeWithSheets(OutputStream outputStream) {
        return new EasyExcelWriteTool(outputStream);
    }

    /**
     * 多个sheet页的数据链式写入（失败了会返回一个有部分数据的Excel）
     *
     * @param response
     * @param exportFileName 导出的文件名称
     */
    public static EasyExcelWriteTool writeWithSheetsWeb(HttpServletResponse response, String exportFileName) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");

        // 这里URLEncoder.encode可以防止中文乱码
        String fileName = URLEncoder.encode(exportFileName, "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ExcelTypeEnum.XLSX);

        return new EasyExcelWriteTool(response.getOutputStream());
    }

    /**
     * 设置响应对象
     *
     * @param response
     * @param fileName
     */
    private static void setResponse(HttpServletResponse response, String fileName) throws UnsupportedEncodingException {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码
        String encodeFileName = URLEncoder.encode(fileName, "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + encodeFileName.replaceAll("\\+", "%20") + ExcelTypeEnum.XLSX);
    }
}
