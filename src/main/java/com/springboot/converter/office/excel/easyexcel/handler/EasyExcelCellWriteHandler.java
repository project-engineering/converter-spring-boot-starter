package com.springboot.converter.office.excel.easyexcel.handler;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import com.alibaba.fastjson2.JSONObject;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.util.ObjectUtils;
import org.springframework.util.PropertyPlaceholderHelper;
import org.springframework.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * 动态设置表头字段
 */
public class EasyExcelCellWriteHandler implements CellWriteHandler {
    private JSONObject headTitle;

    PropertyPlaceholderHelper placeholderHelper = new PropertyPlaceholderHelper("${", "}");

    public EasyExcelCellWriteHandler(JSONObject headTitle) {
        this.headTitle = headTitle;
    }

    @Override
    public void beforeCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row, Head head, Integer columnIndex, Integer relativeRowIndex, Boolean isHead) {
        // 动态设置表头字段
        if (!ObjectUtils.isEmpty(head)) {
            List<String> headNameList = head.getHeadNameList();
            if (!CollectionUtils.isEmpty(headNameList)) {
                ArrayList<Properties> propertiesList = new ArrayList<>();
                for (String key : headTitle.keySet()) {
                    Properties properties = new Properties();
                    properties.setProperty(key, headTitle.getString(key));
                    propertiesList.add(properties);
                }
                for (int i = 0; i < headNameList.size(); i++) {
                    for (Properties properties : propertiesList) {
                        //表头中如果有${}设置的单元格，则可以自定义赋值。 根据构造方法传入的jsonObject对象
                        headNameList.set(i, placeholderHelper.replacePlaceholders(headNameList.get(i), properties));
                    }
                }
            }
        }
    }
}
