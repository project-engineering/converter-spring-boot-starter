package com.springboot.converter.office.word;

import cn.afterturn.easypoi.word.WordExportUtil;
import cn.hutool.core.util.ObjectUtil;
import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;
import com.springboot.converter.util.DateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.wp.usermodel.HeaderFooterType;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STFldCharType;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import java.io.*;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Word工具类
 */
@Log4j2
public class WordUtil {
    public static final String DOC_TYPE = "DOC";
    public static final String DOCX_TYPE = "DOCX";
    private static final String TEMPLATE_PATH = "templates/word/";
    private static final String TEMPLATE_FILE = "template.docx";
    /**
     * 导出word存放的路径
     */
    private static String LOCAL_PATH;
    /**
     * 导出word的文件名
     */
    private static String FILE_NAME;
    /**
     * 页眉
     */
    private static String HEADER;
    /**
     * 页脚
     */
    private static String FOOTER;

    /**
     * 2003- 版本的word
     */
    private static final String word2003L = ".doc";
    /**
     * 2007+ 版本的word
     */
    private final static String word2007U = ".docx";
    /**
     * pdf 文件后缀
     */
    private final static String PDF = ".pdf";

    /**
     * 导出word文档
     * @param template 模板文件名 如 classpath:/templates/word/template.docx
     * @param localPath 导出路径 如 E:/data/2024-04-11/
     * @param fileName 导出文件名 如 test.docx
     * @param map 数据
     * @param headerText 页眉文字
     * @param fileFormat 文件格式
     */
    public static void exportDocument(String template, String localPath, String fileName, Map<String, Object> map, String headerText, String fileFormat) {
        log.info("开始导出文档，模板[{}]", template);
        try {
            if ("docx".equalsIgnoreCase(fileFormat)) {
                // 导出为docx格式
                exportDocx(template, localPath, fileName, map, headerText);
            } else {
                throw new IllegalArgumentException("不支持的文件格式: " + fileFormat);
            }
            log.info("导出文档成功，导出路径[{}], 文件名[{}]", localPath, fileName);
        } catch (Exception e) {
            log.error("导出文档失败,原因：", e);
            throw new RuntimeException("导出文档失败", e);
        }
    }

    /**
     * 按照word模板导出word
     * @param template 模板文件名 如 classpath:/templates/word/template.docx
     * @param localPath 导出路径 如 E:/data/2024-04-11/
     * @param fileName 导出文件名 如 test.docx
     * @param map 数据
     */
    public static void exportDocx(String template, String localPath, String fileName, Map<String, Object> map, String headerText) {
        log.info("开始导出word，模板[{}]", template);
        try {
            String url = getTemplateFile(template).getAbsolutePath();
            String filePath;
            if (ObjectUtils.isEmpty(localPath)) {
                filePath = Paths.get(localPath, fileName).toString();
            } else {
                filePath = getPathByName(fileName);
            }
            File file = new File(filePath);
            if (!file.exists()) {
                // 创建文件夹
                File parentDir = file.getParentFile();
                if (!parentDir.exists() && !parentDir.mkdirs()) {
                    log.error("创建文件夹失败，路径[{}]", parentDir.getAbsolutePath());
                    throw new IOException("创建文件夹失败");
                }

                if (!file.createNewFile()) {
                    log.error("创建文件失败，路径[{}]", file.getAbsolutePath());
                    throw new IOException("创建文件失败");
                }
            }
            // 导出word
            XWPFDocument doc = WordExportUtil.exportWord07(url, map);

            // 添加页眉
            addHeader(doc, headerText);

            // 添加页脚
            addFooter(doc);

            try (FileOutputStream fos = new FileOutputStream(filePath)) {
                doc.write(fos);
            }
        } catch (Exception e) {
            log.error("导出word失败,原因：", e);
            throw new RuntimeException("导出word失败", e);
        }
        log.info("导出word成功，导出路径[{}], 文件名[{}]", localPath, fileName);
    }


    private static File getTemplateFile(String template) throws IOException {
        Resource resource =new ClassPathResource(TEMPLATE_PATH+template);
        return  resource.getFile();
    }

    /**
     * 通过文件名获取文件路径
     * @param fileName
     * @return
     */
    public static String getPathByName(String fileName){
        return "E:\\data\\"+ DateUtil.covertObjToString(DateUtil.getCurrDateTime(),DateUtil.DATE_FORMATTER_3)+"\\"+fileName;
    }

    /**
     * 添加页眉
     * @param doc 文档
     * @param headerText 页眉文字
     */
    private static void addHeader(XWPFDocument doc,String headerText) {
        XWPFHeader header = doc.createHeader(HeaderFooterType.DEFAULT);
        XWPFParagraph paragraphHeader = header.getParagraphArray(0);
        if (paragraphHeader == null) {
            paragraphHeader = header.createParagraph();
        }
        paragraphHeader.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runHeader = paragraphHeader.createRun();
        runHeader.setText(headerText);
    }

    /**
     * 添加页脚
     * @param doc 文档
     */
    private static void addFooter(XWPFDocument doc) {
        // 添加页脚
        XWPFFooter footer = doc.createFooter(HeaderFooterType.DEFAULT);
        XWPFParagraph paragraphFooter = footer.createParagraph();
        paragraphFooter.setAlignment(ParagraphAlignment.CENTER);

        // 添加 "第" 字
        XWPFRun runFirst = paragraphFooter.createRun();
        runFirst.setText("第");

        // 添加页码
        XWPFRun pageNumberRun = paragraphFooter.createRun();
        pageNumberRun.getCTR().addNewFldChar().setFldCharType(STFldCharType.BEGIN);
        pageNumberRun.getCTR().addNewInstrText().setStringValue("PAGE \\* MERGEFORMAT");
        pageNumberRun.getCTR().addNewFldChar().setFldCharType(STFldCharType.SEPARATE);
        pageNumberRun.getCTR().addNewFldChar().setFldCharType(STFldCharType.END);
        // 添加 "页" 字
        XWPFRun runLast = paragraphFooter.createRun();
        runLast.setText("页");
    }

    /**
     * 将Word文档转换为PDF
     *
     * <p>
     * 支持 Word2003、Word2007+ 版本的Word文档转换为PDF
     * <p>
     * 注意：
     *    通过documents4j 实现word转pdf -- linux 环境 需要有 libreoffice 服务
     *
     * @param inputWordFile word文件地址 如 /root/example.doc
     * @param outputPdfFile pdf文件存放地址 如 /root/example.pdf
     */
    public static void convertWordToPdf(String inputWordFile, String outputPdfFile) {
        log.info("开始将word文件转换为pdf文件,word文件[{}]", inputWordFile);
        if (ObjectUtil.isEmpty(inputWordFile) || ObjectUtil.isEmpty(outputPdfFile)){
            throw new IllegalArgumentException("输入参数不能为空");
        }
        if (!inputWordFile.endsWith(word2003L) &&!inputWordFile.endsWith(word2007U)) {
            throw new IllegalArgumentException("输入文件格式不正确，仅支持doc和docx格式");
        }
        if (!outputPdfFile.endsWith(PDF)) {
            throw new IllegalArgumentException("输出文件格式不正确，仅支持pdf格式");
        }
        InputStream docxInputStream = null;
        OutputStream outputStream = null;
        IConverter converter = LocalConverter.builder().build();
        try {
            docxInputStream = new FileInputStream(inputWordFile);
            outputStream = new FileOutputStream(outputPdfFile);

            if (inputWordFile.endsWith(word2003L)) {
                converter.convert(docxInputStream)
                        .as(DocumentType.DOC)
                        .to(outputStream)
                        .as(DocumentType.PDF).execute();
            }
            if (inputWordFile.endsWith(word2007U)) {
                converter.convert(docxInputStream)
                        .as(DocumentType.DOCX)
                        .to(outputStream)
                        .as(DocumentType.PDF)
                        .execute();
            }
            log.info("word文件转换为pdf文件完成,pdf文件:[{}]",outputPdfFile);
        } catch (Exception e) {
            log.error("word转pdf失败:{}", e.getMessage());
            throw new RuntimeException("word转pdf失败:" + e.getMessage(), e);
        } finally {
            if (converter != null) {
                try {
                    converter.shutDown();
                } catch (Exception e) {
                    log.error("关闭转换器失败:{}", e.getMessage());
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    log.error("关闭输出流失败:{}", e.getMessage());
                }
            }
            if (docxInputStream != null) {
                try {
                    docxInputStream.close();
                } catch (IOException e) {
                    log.error("关闭输入流失败:{}", e.getMessage());
                }
            }
        }
    }
}
