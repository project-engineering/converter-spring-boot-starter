package com.springboot.converter.lang;

/**
 * @author liuc
 */
public interface IsEmpty {
    boolean isEmpty();
}
